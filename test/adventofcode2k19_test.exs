defmodule Adventofcode2k19Test do
  use ExUnit.Case

  test "day 2" do
    assert Day2.part1 == 12490719
    assert Day2.part2 == 2003
  end

  test "day 5" do
    assert Day5.part1 == 10987514
    assert Day5.part2 == 14195011
  end

  test "day 7" do
    assert Day7.part1 == 20413
    assert Day7.part2 == 3321777
  end
end
