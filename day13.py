from intcode_program import Computer
import itertools

def create_coordinates_representation(computer):
    raw_data = {tile: [] for tile in range(5)}
    x = 0
    y = 0
    while not computer.stop:
        x = computer.run()
        y = computer.run()
        output = computer.run()
        if output in raw_data:
            raw_data[output].append((x, y))
        else:
            raw_data[output] = [(x, y)]
    return raw_data

def play_game(computer):
    x = y = ball_x = paddle_x = score = 0
    computer.program[0] = 2
    computer.set_input(lambda : (ball_x > paddle_x) - (ball_x < paddle_x))

    while not computer.stop:
        x = computer.run()
        y = computer.run()
        output = computer.run()
        if (x, y) == (-1, 0):
            score = output
        elif output == 3:
            paddle_x = x
        elif output == 4:
            ball_x = x

    return score


if __name__ == "__main__":
    computer = Computer("./inputs/day13.txt", memory_extension=10000, external_handler=True)
    raw_data = create_coordinates_representation(computer)
    print(count_tile_type(raw_data, 2))
    computer.reset()
    print(play_game(computer))
