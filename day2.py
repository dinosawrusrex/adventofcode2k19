from intcode_program import parse_input, Computer

def program(raw_program, noun=12, verb=2):
    program = raw_program.copy()
    program[1], program[2] = noun, verb
    return program

def brute_force_find_noun_and_verb(raw_program, expected_output):
    for noun in range(0, 99):
        for verb in range(0, 99):
            modified_program = program(raw_program, noun, verb)
            com = Computer(modified_program)
            com.run()
            if com.program[0] == expected_output:
                return 100 * noun + verb


if __name__ == "__main__":
    raw_program = parse_input("./inputs/day2.txt")
    computer = Computer(program(raw_program))
    computer.run()
    print(computer.program[0])
    print(brute_force_find_noun_and_verb(raw_program, 19690720))
