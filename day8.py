import more_itertools

def parse_input(file):
    return [int(pixel) for pixel in file.readline().strip()]

def layers_1d(images, **kwargs):
    width = kwargs["width"]
    height = kwargs["height"]
    return more_itertools.chunked(images, width*height)

def layers_2d(images, **kwargs):
    width = kwargs["width"]
    return [list(more_itertools.chunked(layer, width))
        for layer in layers_1d(images, **kwargs)
    ]

def multiply_count_1_count_2_from_fewest_0_layer(layers_1d):
    layer = min(layers_1d, key=lambda layer: layer.count(0))
    return layer.count(1) * layer.count(2)

def image(layers):
    for layer in layers:
        for row in layer:
            if row == 0:
                print(" ", end=" ")
            elif row == 1:
                print("#", end=" ")
            else:
                print("t", end=" ")
        print()

def process_image(images, **kwargs):
    layers = layers_2d(images, **kwargs)
    processed_layer = [
        [0 for width in range(kwargs["width"])]
        for height in range(kwargs["height"])
    ]
    for height in range(len(processed_layer)):
        for width in range(len(processed_layer[height])):
            for layer in layers:
                if layer[height][width] in [0, 1]:
                    processed_layer[height][width] = layer[height][width]
                    break
    return processed_layer


if __name__ == "__main__":
    with open("./inputs/day8.txt", "r") as file:
        images = parse_input(file)

    image_specification = {"width": 25, "height": 6}

    print(multiply_count_1_count_2_from_fewest_0_layer(
        layers_1d(images, **image_specification)
    ))
    print(image(process_image(images, **image_specification)))

    # Answer checking
    assert multiply_count_1_count_2_from_fewest_0_layer(
        layers(images, **image_specification)
    ) == 2806
