import math, more_itertools

def parse_input(path_or_program, memory_extension=0):
    if isinstance(path_or_program, list):
        return list(path_or_program) + [0] * memory_extension
    with open(path_or_program, "r") as raw_program:
        program = [
            int(number.strip()) for number in raw_program.readline().split(",")
        ]
    return program + [0] * memory_extension

class Computer:
    def __init__(
        self,
        path_or_program,
        user_input=0,
        external_handler=False,
        memory_extension=0,
        debug=False,
        id=0,
    ):

        self.original_program = parse_input(path_or_program, memory_extension)
        self.program = list(self.original_program)

        self.user_input = user_input

        self.external_handler = external_handler
        self.id = id
        self.debug = debug

        self.index = 0
        self.relative_base = 0
        self.output = 0
        self.opcode = 0
        self.parameter_modes = []
        self.indices = []
        self.stop = False

    def _update_opcode_and_parameter_modes(self):
        instruction = self.program[self.index]

        if instruction == 99:
            self.stop = True
            return

        instruction = [int(value) for value in str(instruction)]

        self.opcode = instruction.pop()
        if instruction: instruction.pop()
        instruction.reverse()

        if self.opcode in [1, 2, 7, 8]:
            padding = 3
        elif self.opcode in [5, 6]:
            padding = 2
        elif self.opcode in [3, 4, 9]:
            padding = 1
        else:
            raise ValueError(f"Invalid opcode '{opcode}' in index {index}")

        self.parameter_modes = list(more_itertools.padded(instruction, 0, padding))

    def _update_addresses(self):
        self.indices = []

        for mode in self.parameter_modes:
            self.index += 1
            if mode == 0:
                self.indices.append(self.program[self.index])
            elif mode == 1:
                self.indices.append(self.index)
            elif mode == 2:
                self.indices.append(self.relative_base + self.program[self.index])
            else:
                raise ValueError(
                    f"Invalid parameter mode {mode} in index {self.index}"
                )
        self.index += 1

    def _add(self):
        self.program[self.indices[-1]] = sum(
            self.program[index] for index in self.indices[:-1]
        )

    def _product(self):
        self.program[self.indices[-1]] = math.prod(
            self.program[index] for index in self.indices[:-1]
        )

    def _user_input(self):
        if isinstance(self.user_input, int):
            self.program[self.indices[0]] = self.user_input
        elif isinstance(self.user_input, list):
            if not self.user_input:
                return "pausing"
            self.program[self.indices[0]] = self.user_input.pop(0)
        elif callable(self.user_input):
            output = self.user_input()
            self.program[self.indices[0]] = output

    def _output(self):
        self.output = self.program[self.indices[0]]
        if self.debug:
            print(self.output)
        if self.external_handler:
            return self.output

    def _jump_if_true(self):
        if self.program[self.indices[0]] != 0:
            self.index = self.program[self.indices[1]]

    def _jump_if_false(self):
        if self.program[self.indices[0]] == 0:
            self.index = self.program[self.indices[1]]

    def _less_than(self):
        if self.program[self.indices[0]] < self.program[self.indices[1]]:
            self.program[self.indices[-1]] = 1
        else:
            self.program[self.indices[-1]] = 0

    def _equals(self):
        if self.program[self.indices[0]] == self.program[self.indices[1]]:
            self.program[self.indices[-1]] = 1
        else:
            self.program[self.indices[-1]] = 0

    def _change_relative_base(self):
        self.relative_base += self.program[self.indices[0]]

    def _act(self):
        return {
            1: self._add,
            2: self._product,
            3: self._user_input,
            4: self._output,
            5: self._jump_if_true,
            6: self._jump_if_false,
            7: self._less_than,
            8: self._equals,
            9: self._change_relative_base,
        }[self.opcode]()

    def run(self):
        while True:
            self._update_opcode_and_parameter_modes()
            if self.stop:
                break
            self._update_addresses()
            if not (output := self._act()) is None:
                return output
        return self.output

    def reset(self):
        self.program = list(self.original_program)
        self.index = 0
        self.relative_base = 0
        self.output = 0
        self.stop = False

    def set_input(self, input_to_set, to_append=False):
        if to_append:
            self.user_input.append(input_to_set)
        else:
            self.user_input = input_to_set
