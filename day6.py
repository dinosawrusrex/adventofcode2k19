def parse_input(file):
    return [orbit.strip().split(")")
        for orbit in file.readlines()
    ]

def collect_space_objects(orbital_relationships):
    space_objects_dict = {}
    space_objects = []
    for pair in orbital_relationships:
        space_objects_dict.setdefault(
            pair[0],
            SpaceObject(pair[0])
        )
        space_objects_dict.setdefault(
            pair[1],
            SpaceObject(pair[1])
        )
        space_objects_dict[pair[0]].orbit_by.append(
            space_objects_dict[pair[1]]
        )
        space_objects_dict[pair[1]].orbits = space_objects_dict[
            pair[0]
        ]

        space_objects.append(space_objects_dict[pair[1]])
    return space_objects_dict, space_objects

class SpaceObject:

    def __init__(self, name):
        self.name = name
        self.orbits = None
        self.orbit_by = []

    def __repr__(self):
        return self.name

    def path_to_orbitee(self, name):
        path = []
        if self.orbits is None:
            return False
        elif self.orbits.name == name:
            path.extend([self.name, name])
            return path
        elif (part_of_path := self.orbits.path_to_orbitee(name)):
            path.append(self.name)
            path.extend(part_of_path)
        return path

    def path_to_orbiting(self, name):
        path = []
        if not self.orbit_by:
            return False
        elif any(space_object.name == name for space_object in self.orbit_by):
            path.extend([self.name, name])
            return path

        for space_object in self.orbit_by:
            if (part_of_path := space_object.path_to_orbiting(name)):
                path.append(self.name)
                path.extend(part_of_path)
        return path

    def path_to_object(self, name):
        path = []
        if self.orbits is None or not self.orbit_by:
            return False
        elif self.orbits.name == name or any(space_object.name == name
            for space_object in self.orbit_by
        ):
            path.extend([self.name, name])
            return path
        elif (part_of_path := self.orbits.path_to_orbiting(name)):
            path.append(self.name)
            path.extend(part_of_path)
        elif (part_of_path := self.orbits.path_to_object(name)):
            path.append(self.name)
            path.extend(part_of_path)
        return path

def total_orbits_around_COM(space_objects):
    orbits = 0
    for space_object in space_objects:
        if (path := space_object.path_to_orbitee("COM")):
            orbits += len(path) - 1
    return orbits

def orbital_transfers_to_santa(space_objects_dict):
    starting_space_object = space_objects_dict["YOU"].orbits
    return len(starting_space_object.path_to_object("SAN")) - 2


if __name__ == "__main__":
    with open("./inputs/day6.txt", "r") as file:
        orbital_relationships = parse_input(file)

    space_objects_dict, space_objects = collect_space_objects(
        orbital_relationships
    )

    print(total_orbits_around_COM(space_objects))
    print(orbital_transfers_to_santa(space_objects_dict))

    # Answer checking
    assert total_orbits_around_COM(space_objects) == 314247
    assert orbital_transfers_to_santa(space_objects_dict) == 514
