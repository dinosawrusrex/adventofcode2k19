require Computer

defmodule Day2 do

  @memory Computer.parse("inputs/day2.txt")

  def part1() do
     Map.get(Computer.run(@memory |> Map.merge(%{1 => 12, 2 => 2})), 0)
  end

  def part2() do
    Stream.flat_map(0..99, fn noun -> Stream.map(0..99, fn verb -> {noun, verb} end) end)
    |> Enum.reduce_while({}, fn(noun_verb, _) ->
      {noun, verb} = noun_verb
      if Map.get(Computer.run(@memory |> Map.merge(%{1 => noun, 2 => verb})), 0) == 19690720 do
        {:halt, {noun, verb}}
      else
        {:cont, {}}
      end
    end)
    |> then(fn({noun, verb}) -> 100*noun+verb end)
  end
end
