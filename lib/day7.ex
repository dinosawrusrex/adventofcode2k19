require Permutation

defmodule Day7 do
  @memory Computer.parse("inputs/day7.txt")

  defp amplify(phases) do
    me = self()

    spawn(fn ->
      Process.register(self(), :a)
      send(self(), Enum.at(phases, 0))
      :timer.sleep(1)
      send(:a, 0)
      Computer.run(Map.put(@memory, :ext, :b))
    end)
    spawn(fn ->
      Process.register(self(), :b)
      send(self(), Enum.at(phases, 1))
      Computer.run(Map.put(@memory, :ext, :c))
    end)
    spawn(fn ->
      Process.register(self(), :c)
      send(self(), Enum.at(phases, 2))
      Computer.run(Map.put(@memory, :ext, :d))
    end)
    spawn(fn ->
      Process.register(self(), :d)
      send(self(), Enum.at(phases, 3))
      Computer.run(Map.put(@memory, :ext, :e))
    end)
    spawn(fn ->
      Process.register(self(), :e)
      send(self(), Enum.at(phases, 4))
      send(me, Computer.run(Map.put(@memory, :ext, :a)))
    end)

    receive do
      out -> Map.get(out, :output)
    end
  end

  defp find_max_from_permutation(phases) do
    Permutation.permute!(Enum.to_list(phases)) |> Stream.map(&amplify/1) |> Enum.max()
  end

  def part1(), do: find_max_from_permutation(0..4)
  def part2(), do: find_max_from_permutation(5..9)
end
